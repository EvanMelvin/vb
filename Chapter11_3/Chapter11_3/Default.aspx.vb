﻿Public Class _Default
    Inherits System.Web.UI.Page

    Private Property NumChecks As Integer
    Private Property Fees As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        NumChecks = TextBox1.Text

        If Integer.TryParse(TextBox1.Text, NumChecks) = False Then
            Label3.Text = "Please enter a positive number."
        ElseIf NumChecks < 0 Then
            Label3.Text = "Please enter a positive number."
        Else
            If NumChecks < 20 Then
                Fees = 10 + (NumChecks * 0.1)
            ElseIf NumChecks > 19 And NumChecks < 39 Then
                Fees = 10 + (NumChecks * 0.08)
            ElseIf NumChecks > 39 And NumChecks < 50 Then
                Fees = 10 + (NumChecks * 0.06)
            ElseIf NumChecks >= 60 Then
                Fees = 10 + (NumChecks * 0.04)
            End If

            Fees = Format(Fees, "0.00")

            Label3.Text = "$" & Fees

        End If
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        Label3.Text = ""
    End Sub
End Class