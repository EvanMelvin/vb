﻿Public Class Form2

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Form1.AddressList.Contains(Me.TextBox1.Text) And Form1.ListBox1.SelectedItem IsNot Me.TextBox1.Text Then
            MsgBox("There is already an entry in the address book for this name.", MsgBoxStyle.Exclamation, "Duplicate Entry")
        Else
            If Form1.EditFlag = True Then
                Form1.AddressList.Remove(Form1.ListBox1.SelectedItem.ToString)
                Form1.ListBox1.Items.Remove(Form1.ListBox1.SelectedItem)
            End If
            Form1.AddressList.Add(New Address(Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox3.Text, Me.RichTextBox1.Text), Me.TextBox1.Text)
            Form1.ListBox1.Items.Add(Form1.AddressList.Item(Me.TextBox1.Text).GetName)
            Form1.EditFlag = False
            Me.Close()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

End Class