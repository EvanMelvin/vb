﻿Public Class Address
    Dim Name As String
    Dim EmailAddress As String
    Dim PhoneNumber As String
    Dim Notes As String

    Public Sub New(Name, EmailAddress, PhoneNumber, Notes)
        Me.Name = Name
        Me.EmailAddress = EmailAddress
        Me.PhoneNumber = PhoneNumber
        Me.Notes = Notes
    End Sub

    Function GetName() As String
        Return Me.Name
    End Function

    Function GetEmail() As String
        Return Me.EmailAddress
    End Function

    Function GetPhoneNum() As String
        Return Me.PhoneNumber
    End Function

    Function GetNotes() As String
        Return Me.Notes
    End Function

    Function SetName(Name As String)
        Me.Name = Name
    End Function

    Function SetEmail(Email As String)
        Me.EmailAddress = Email
    End Function

    Function SetPhoneNum(Phone As String)
        Me.PhoneNumber = Phone
    End Function

    Function SetNotes(Notes As String)
        Me.Notes = Notes
    End Function

End Class
