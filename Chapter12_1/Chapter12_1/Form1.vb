﻿Public Class Form1

    Private Property AddressBox As ListBox
    Public Property AddressList As Collection
    Public Property EditFlag As Boolean


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim AddressForm As New Form2
        EditFlag = False
        AddressForm.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Me.ListBox1.SelectedItem IsNot Nothing Then
            Dim AddressForm As New Form2
            AddressForm.Show()
            AddressForm.TextBox1.Text = AddressList.Item(Me.ListBox1.SelectedItem.ToString).GetName()
            AddressForm.TextBox2.Text = AddressList.Item(Me.ListBox1.SelectedItem.ToString).GetEmail()
            AddressForm.TextBox3.Text = AddressList.Item(Me.ListBox1.SelectedItem.ToString).GetPhoneNum()
            AddressForm.RichTextBox1.Text = AddressList.Item(Me.ListBox1.SelectedItem.ToString).GetNotes()
            EditFlag = True
        Else
            MsgBox("No address selected.", MsgBoxStyle.Exclamation, "Error")
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Me.ListBox1.SelectedItem IsNot Nothing Then
            Me.AddressList.Remove(Me.ListBox1.SelectedItem.ToString)
            Me.ListBox1.Items.Remove(Me.ListBox1.SelectedItem)
        Else
            MsgBox("No address selected.", MsgBoxStyle.Exclamation, "Error")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class
